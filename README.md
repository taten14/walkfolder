# walkfolder

Script creado con el fin de analizar las carpetas de Rendicion de Cuentas.

# Tecnologias
- Python
- La informacion se almacena en archivos tipo logs. Los que son adjuntados en el email
- MD5 SHA256

## Chequeos
- Chequea que los archivos PDFs sean tipo A
- Chequea que tengan un nombre especifico
- Chequea Firma deigital
- Busca archivos repetidos
- Genera un informe que se envia via mail
- Omite archivos que no se desean analizar

## Mejoras planteada
- [ ] Usar la libreria logs en lugar de la libreria personalizada
- [ ] Comprimir los archivos antes de enviarlos
- [ ] HTML en el formato del email
- [ ] Colores institucionales
- [ ] Adjuntar un archivo separado para la lista de archivos que tienen error
- [ ] Usar base de datos en lugar de logs