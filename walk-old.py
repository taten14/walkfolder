#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
#import sys
#import re
import datetime
import time
import csv
import yaml
from email.mime.multipart import MIMEMultipart 
from email.mime.text import MIMEText 
from email.mime.base import MIMEBase 
from email import encoders
import smtplib
import hashlib


def walkFolder(folder):
    if not os.path.isdir(folder) | os.path.ismount(folder):
            print('[walkFolder] Error: El directorio {} no existe o no esta montado'.format(folder))
    else:
        try:
            walkFolderObject = os.walk(folder)
        except Exception as e:
            print('[walkFolder] Error: {}'.format(type(e).__name__))
            # Mandarme mail a mi porque fallo
        else:
            return walkFolderObject


def walkFoldertoList(walkFolderObject):
    limpiarCadena = lambda x: x.encode('unicode-escape').decode().replace('\\\\', '\\')
    getMTime = lambda x: datetime.datetime.fromtimestamp(os.path.getmtime(x))
    getCTime = lambda x: datetime.datetime.fromtimestamp(os.path.getctime(x))
    walkFolderList=[['Carpeta','Archivo','Creacion','Modificacion','MD5']]
    totalTime=time.time()
    totalFiles=0
    t0=time.time()
    for folderTuple in walkFolderObject:
        print('[walkFoldertoList] Iniciando analisis de subcarpeta : {}'.format(limpiarCadena(folderTuple[0])))
        if len(folderTuple[2])==0 and len(folderTuple[1])==0:
            walkFolderList.append([limpiarCadena(folderTuple[0]), '', getCTime(folderTuple[0]).__str__(), getMTime(folderTuple[0]).__str__(), ''])
        fileCounter=0
        for folderFile in folderTuple[2]:
            totalFiles+=1
            fileCounter+=1
            fName='\\'.join([folderTuple[0], folderFile])
            walkFolderList.append([limpiarCadena(folderTuple[0]), limpiarCadena(folderFile), getCTime(fName).__str__(), getMTime(fName).__str__(),getMD5Hash(folderTuple[0], folderFile)])
        print('[walkFoldertoList] Analisis de subcarpeta Finalizado. Tiempo: {:.0f} segundos. Archivos: {}'.format(time.time()-t0,fileCounter))
        fileCounter=0
        t0=time.time()
    print('[walkFoldertoList] Tiempo total transcurrido: {:.0f} segundos. Total de archivos procesados: {}'.format(time.time()-totalTime,totalFiles))
    return walkFolderList


def walkFolderToCSV(walkFolderList,filename=None):
    fecha=datetime.datetime.now().strftime("%Y%m%d")
    fileName = '{}-carpeta.csv'.format(fecha) if filename == None else '{}-{}.csv'.format(fecha,filename)
    try:
        with open(fileName,'w+',newline ='',encoding='utf-8') as csvFile:
            csvWriter=csv.writer(csvFile)
            csvWriter.writerows(walkFolderList)
    except Exception as e:
        print('[walkFolderToCSV] Error: {}'.format(type(e).__name__))
    else:
        print('[walkFolderToCSV] Archivo {} creado correctamente'.format(filename))
        return [fileName,os.path.abspath(fileName)]

def getMD5Hash(path,filename):
    #Necesito saber que carpeta se esta procesando del archivo carpetas.yml
    #Con esto tomo el nuevo field extensiones y filtro solo esas. Si el campo esta vacio se analizaran todos los archivos
    extensiones=getwalkCarpetas()
    try:
        print('[getMD5Hash] Generando MD5 para {}'.format(filename))
        hash_md5 = hashlib.md5()
        with open('{}\\{}'.format(path, filename), 'rb') as punteroBinario:
            for chunk in iter(lambda: punteroBinario.read(4096), b""):
                hash_md5.update(chunk)
        print('[getMD5Hash] hash: {}. Finalizado...'.format(hash_md5.hexdigest()))
        return hash_md5.hexdigest()
    except Exception as e:
        print('[getMD5Hash] Error {} al procesar archivo {}'.format(type(e).__name__,filename))
        return 'MD5 Error: {}'.format(type(e).__name__)

def getwalkCarpetas(file='.\\carpetas.yml'):
    try:
        with open(file,'r',encoding='utf-8') as yamlFilePointer:
            walkCarpetas = yaml.load(yamlFilePointer, Loader=yaml.FullLoader)
    except Exception as e:
        print('[getwalkCarpetas] Error: {}'.format(type(e).__name__))
    else:
        return walkCarpetas

def getDuplicates(walkFolderList):
    #Recibe la lista co,mpleta de archivos
    # devuelve una lista de archivos duplicados
    listaDuplicados = lambda fileMD5,walkFolderList: [[file] for file in walkFolderList if file[4] == fileMD5 and not fileMD5 == '']
    return [[dupFile] for dupFile in walkFolderList if len(listaDuplicados(dupFile[4],walkFolderList))>1]

def setBodyDuplicates(duplicatesList):
    pass
    
def getMailCredentials(file=".\\mailconfig.yml"):
    try:
        with open(file,'r',encoding='utf-8') as yamlFilePointer:
            mailConfigDict = yaml.load(yamlFilePointer, Loader=yaml.FullLoader)
    except Exception as e:
        print('[getMailCredentials] Error: {}'.format(type(e).__name__))
    else:
        return mailConfigDict


def getMailSettings(file='.\\body.yml'):
    try:
        with open(file,'r',encoding='utf-8') as yamlFilePointer:
            bodyConfigDict = yaml.load(yamlFilePointer, Loader=yaml.FullLoader)
    except Exception as e:
        print('[getMailSettings] Error: {}'.format(type(e).__name__))
    else:
        return bodyConfigDict


def setEmailDict(folder,para=['dante.paniagua@tcpcordoba.gov.ar','taten14@gmail.com'],\
        de='alertas@tcpcordoba.gov.ar',\
        asunto='[Prueba] prueba',\
        cuerpo='Prueba',\
        filename=None):
    print('[setEmailDict] Generando la configuracion del Email...')
    walkFolderObject = walkFolder(folder)
    walkFolderList = walkFoldertoList(walkFolderObject)
    csvFileList=walkFolderToCSV(walkFolderList,filename)
    mailSettings=getMailSettings()
    emailDict={}
    emailDict['from']=de
    emailDict['to']=para
    emailDict['subject']=asunto
    emailDict['body']='{}'.format(cuerpo)
    emailDict['body-encode']=mailSettings['email']['settings']['body-encode']
    emailDict['body-type']=mailSettings['email']['settings']['body-type']
    emailDict['attach'] = [csvFileList]
    return emailDict


def MimeFromYYaml(emailDict):
    msgMIMEMultipart = MIMEMultipart()
    msgMIMEMultipart['From']=emailDict['from']
    msgMIMEMultipart['To']=','.join(emailDict['to'])
    msgMIMEMultipart['Subject']=emailDict['subject']
    try:
        msgMIMEMultipart.attach(MIMEText(emailDict['body'],emailDict['body-type'],emailDict['body-encode']))
    except Exception as e:
        print('[MimeFromYYaml] Error adjuntando el cuerpo del mensaje. Error: {}'.format(e))
    else:    
        if len(emailDict['attach']) > 0:
            for archivo in emailDict['attach']:
                try:
                    attachment=open(archivo[1],'rb')
                    p = MIMEBase('application', 'octet-stream')
                    p.set_payload((attachment).read())
                    encoders.encode_base64(p)
                    p.add_header('Content-Disposition', "attachment; filename= {}".format(archivo[0]))
                except Exception as e:
                    print('[MimeFormatYaml] No se pudo adjuntar el archivo {}. Error {}'.format(archivo[1],e))
                else:
                    msgMIMEMultipart.attach(p)
    return msgMIMEMultipart


def enviarEmail(cuerpo=MIMEMultipart()):
    emails=cuerpo['To'].split(',')
    contador=0
    mailCredentials = getMailCredentials()
    for email in emails:
        contador+=1
        print('[enviarEmail] Enviando email a {} {} / {} emails enviados'.format(email,contador,len(emails)))
        server = smtplib.SMTP(mailCredentials['credentials']['smtp']['host'],int(mailCredentials['credentials']['smtp']['port']))
        server.ehlo()
        server.starttls()
        server.login(mailCredentials['credentials']['smtp']['user'], mailCredentials['credentials']['smtp']['pass'])
        server.sendmail(cuerpo['From'],email,cuerpo.as_string())


def main():
    walkCarpetas=getwalkCarpetas()
    for carpeta in walkCarpetas['carpetas']:
        asunto = walkCarpetas['carpetas'][carpeta]['subject']+' del {}'.format(datetime.datetime.now().strftime("%d-%m-%Y"))
        cuerpo = '''{}
Carpeta: {}
Fecha: {}'''.format(walkCarpetas['carpetas'][carpeta]['subject'],walkCarpetas['carpetas'][carpeta]['path'],datetime.datetime.now().strftime("%d-%m-%Y"))
        csvFileName='_'.join(walkCarpetas['carpetas'][carpeta]['subject'].lower().split())
        emailDict=setEmailDict(walkCarpetas['carpetas'][carpeta]['path'],\
            walkCarpetas['carpetas'][carpeta]['to'],\
            asunto=asunto,\
            cuerpo=cuerpo,\
            filename=csvFileName)
        enviarEmail(MimeFromYYaml(emailDict))

if __name__ == "__main__":
    main()