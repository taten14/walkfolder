# -*- coding: utf-8 -*-
#!/usr/bin/python3

import os
from sys import exit as sysexit
import re
import datetime
import time
import csv
from types import GetSetDescriptorType
import yaml
from email.mime.multipart import MIMEMultipart 
from email.mime.text import MIMEText 
from email.mime.base import MIMEBase 
from email import encoders
import smtplib
import hashlib
from collections import Counter
import platform
from mods.archivo import archivo

class configuraciones():
    def __init__(self,bodyConfigFile='./body.yml',walkConfigFile='./carpetas.yml',carpetasConfigFile='./body.yml',appSettingsFile='./appsettings.yml',mailSettingsFile='./mailconfig.yml'):
        self.bodyConfig = self.__getSettings(bodyConfigFile)
        self.walkConfig = self.__getSettings(walkConfigFile)
        self.appSettings = self.__getSettings(appSettingsFile)
        self.mailconfig = self.__getSettings(mailSettingsFile)
    def __str__(self):
        return('{}\n{}\n{}\n{}'.format(self.bodyConfig,self.walkConfig,self.walkConfig,self.appSettings))
    def __getSettings(self,file):
        try:
            print('[getSettings] Cargando archivo de configuracion {}'.format(file))
            if platform.system()=='Windows':
                with open(file,'r',encoding='utf-8') as yamlFilePointer:
                    appSettings = yaml.load(yamlFilePointer, Loader=yaml.FullLoader)
            else:
                with open(file,'r',encoding='utf-8') as yamlFilePointer:
                    appSettings = yaml.load(yamlFilePointer)
        except Exception as e:
            print('[getSettings] Error: {}'.format(type(e).__name__))
        else:
            return appSettings

class logToFile():
    def __init__(self,folder,filename):
        self.folder = folder
        self.logFileName = self.getFileName(filename)
        self.__folderLimiter ='\\' if platform.system()=='Windows' else '/'
    def __str__(self):
        return(f'{self.folder}{self.__folderLimiter}{self.logFileName}')
    def getFileName(self,filename):
        fechaArchivo = datetime.datetime.now().strftime('%Y%m%d')
        return '{}-{}.log'.format(fechaArchivo,filename) if filename.split('.')[-1] != 'log' else '{}-{}'.format(fechaArchivo,filename)
    def setLogFolder(self):
        try:
            print('[createFolder] Intentando crear la carpeta {}.'.format(self.folder))
            os.mkdir(self.folder)
            print('[createFolder] Carpeta {} creada con exito.'.format(self.folder))
        except Exception as e:
            print('[createFolder] Error: {} No se pudo crear la carpeta {}. Saliendo del programa'.format(type(e).__name__,self.folder))
            sysexit() # es una buena practica??
    def setMensaje(self,mensaje):
        if not os.path.isdir(self.folder):
            self.setLogFolder()
        timeStamp = '{}'.format(datetime.datetime.now().strftime('%Y%m%d-%H%M%S'))
        with open(f'{self.folder}{self.__folderLimiter}{self.logFileName}','a+',encoding='utf-8',newline='') as filePointer:
            filePointer.write('[{}] {}\n'.format(timeStamp,mensaje))


class walkFolder():
    def __init__(self,folderDict,logObject):
        self.pdfFIleErrorList = [['Carpeta','Nombre de Archivo']]
        self.pdfErrorFirma = [['Carpeta','Nombre de Archivo']]
        self.pdfErrorNombre = []
        self.pdfRaw = []
        self.pdfNoConformance = []
        self.otherFiles = []
        self.pdfDuplicados = []
        self.filesAttach = []
        self.__folderLimiter = '\\' if platform.system()=='Windows' else '/'
        # Recibe un diccionario de la carpeta a procesar [carpeta][nombre_carpeta] resultado de procesar el archivo carpetas.yml
        self.path=folderDict['path']
        self.extensiones=folderDict['extensiones']
        self.regExpr = folderDict['validador']
        self.winPath = folderDict['winPath']
        self.walkFolderObject = self.__walkFolder()
        self.logObject = logObject
        self.fileList = self.setFileList()
    def __str__(self):
        return '{}'.format(self.body())
    def body(self):
        bodyText = 'Ocurrencia de Archivos(por extensiones): \n\n'
        bodyText += f'Archivos PDF: {len(self.pdfRaw)}\n\n'
        bodyText += f'\tNo Pdf/A: {len(self.pdfNoConformance)}\n'
        bodyText += f'\tPdf con Error de Firmas: {len(self.pdfErrorFirma)}\n'
        bodyText += f'\tPdf con Error de Nombres: {len(self.pdfErrorNombre)}\n'
        bodyText += f'\tPdfs Duplicados: {len(self.pdfDuplicados)}\n\n'
        bodyText += f'Otros Archivos: {len(self.otherFiles)}\n\n'
        listaEtensioneArchivo = Counter([ file[1].split('.')[-1].lower() for file in self.otherFiles ])
        for item in listaEtensioneArchivo:
            bodyText += f'\t{item}: {listaEtensioneArchivo[item]}\n'
        bodyText += f'\nTotal de archivos: {len(self.pdfRaw)+len(self.otherFiles)}\n'
        if len(self.pdfErrorFirma)>1:
            bodyText += f'\n\nArchivos PDF Con error en Firma Digital: {len(self.pdfErrorFirma)}\n\n'
            for file in self.pdfErrorFirma[1:]:
                bodyText += f'{file[0]}\\{file[1]}\n'
        if len(self.pdfNoConformance)>1:
            bodyText += f'\n\nArchivos PDF que no son A: {len(self.pdfNoConformance)}\n\n'
            for file in self.pdfNoConformance[1:]:
                bodyText += f'{file[0]}\\{file[1]}\n'
        if len(self.pdfDuplicados)>1:
            claves = list(Counter([file[4] for file in self.pdfDuplicados[1:]]).keys())
            bodyText += f'\n\nArchivos Duplicados: {len(self.pdfDuplicados)-len(claves)}\n'
            for clave in claves:
                bodyText += f'\nMD5: {clave}:\n'
                for file in self.pdfDuplicados:
                    if file[4] == clave:
                        bodyText += f'{file[0]}\\{file[1]}\n'
        return bodyText
    def __walkFolder(self):
        if not os.path.isdir(self.path) | os.path.ismount(self.path):
            print('[walkFolder] Error: El directorio {} no existe o no esta montado'.format(self.path))
        else:
            try:
                walkFolderObject = os.walk(self.path)
            except Exception as e:
                print('[walkFolder] Error: {}'.format(type(e).__name__))
                # Mandarme mail a mi porque fallo
            else:
                return walkFolderObject
    def __ocurrence(self):
        return dict(Counter([file[1].split('.')[-1].lower() for file in [folder for folder in self.fileList if folder[1]!='']]))
    def __folderFormatter(self,path,replace):
        limpiarCadena = lambda x: '{}'.format(x.encode('unicode-escape').decode().replace('\\\\', '\\'))
        return path.replace('/home/TRIBUNALCBA/dpaniagua/winSectores/Rendicion de Cuentas',limpiarCadena(replace)).replace('/','\\')
    def getOcurrenceText(self):
        ocurrenceText='Archivos:\n\n'
        filesCounter = self.__ocurrence()
        for fileType in filesCounter:
            ocurrenceText+='\t- *.{}: {} archivos\n'.format(fileType,filesCounter[fileType])
        return ocurrenceText
    def setFileList(self):
        fileList = []
        for folderTuple in self.walkFolderObject:
            if len(folderTuple[2])>0:
                for file in folderTuple[2]:
                    fileList.append([folderTuple[0],file])
        return fileList
    def analizeFiles(self):
        for fileFolder in self.fileList:
            print(f'Analizando carpeta: {fileFolder[0]}')
            if fileFolder[1].split('.')[-1].lower() == 'pdf' and 'pdf' in self.extensiones:
                try:
                    print(f'Analizando Archivo: {fileFolder[1]}')
                    pdfFile = archivo(fileFolder[0],fileFolder[1])
                except Exception as e:
                    self.pdfFIleErrorList.append([self.__folderFormatter(fileFolder[0],self.winPath),fileFolder[1]])
                else:
                    print(f'Exito Archivo: {fileFolder[1]}')
                    if not pdfFile.isSigned():
                        self.pdfErrorFirma.append([self.__folderFormatter(fileFolder[0],self.winPath),fileFolder[1]])
                    self.pdfRaw.append([self.__folderFormatter(pdfFile.full_toCsv()[0],self.winPath),pdfFile.full_toCsv()[1],pdfFile.full_toCsv()[2],pdfFile.full_toCsv()[3],pdfFile.full_toCsv()[4],pdfFile.full_toCsv()[5]])
            else:
                self.otherFiles.append([self.__folderFormatter(fileFolder[0],self.winPath),fileFolder[1]])
    def setWrongFileNameList(self):
        '''
        Si es un archivo y esta en la lista de extensiones a analizar comparara el nombre con la regexpr
        almacenada en self.includeList. Almacena esta info en self.listaNombres
        '''
        isIncluded = lambda file,includeList: True if file.split('.')[-1].lower() in includeList else False
        FileList = []
        if len(self.regExpr) > 0:
            for validator in self.regExpr:
                FileList += [[file[0],file[1],file[2],file[3],file[4],validator] for file in self.pdfRaw if (len(file[1])>0 and isIncluded(file[1],self.extensiones))]
        self.pdfErrorNombre = [['Carpeta','Archivo','Creacion','Modificacion','MD5','Filtro']]+FileList
        return self.pdfErrorNombre
    def setPdfNoConformance(self):
        self.pdfNoConformance = [['Carpeta','Archivo']]+[ [file[0],file[1]] for file in  self.pdfRaw if file[5]==None]
        return [['Carpeta','Archivo']]+[ [file[0],file[1]] for file in  self.pdfRaw if file[5]==None]
    def setDuplicates(self):
        '''
        Recibe la lista completa de archivos
        Devuelve una lista de archivos duplicados ordenada por MD5 que setea en self.listaDuplicados
        '''
        listaDuplicados = lambda fileMD5,walkFolderList: [[file] for file in walkFolderList if file[4] == fileMD5 and not fileMD5 == '' and not fileMD5 == None]
        listaOrdenada = [dupFile for dupFile in self.pdfRaw if len(listaDuplicados(dupFile[4],self.pdfRaw))>1]
        self.pdfDuplicados = [['Carpeta','Archivo','Creacion','Modificacion','MD5','PDF Conformance']]+sorted(listaOrdenada,key=lambda x: x[4])
    def listToCSV(self, lista,filename,path='./'): #Modificar. Yo quiero enviar la lista que se va a escribir en el CSV
        if not os.path.isdir(path):
            self.setLogFolder(path)
        fecha=datetime.datetime.now().strftime("%Y%m%d")
        getFname = lambda fname,fecha: '{}-{}.csv'.format(fecha,fname) if fname.split('.')[-1].lower() != 'csv' else '{}-{}'.format(fecha,fname)
        if len(lista) > 1:
            appendInAttachment = lambda file, fileList: True if file in fileList else False
            #getFname = lambda fname,fecha: '{}-{}.csv'.format(fecha,fname) if fname.split('.')[-1].lower() != 'csv' else '{}-{}'.format(fecha,fname)
            try:
                print(f'{path}{getFname(filename,fecha)}')
                with open(f'{path}{getFname(filename,fecha)}','w+',newline ='',encoding='utf-8') as csvFile:
                    csvWriter=csv.writer(csvFile)
                    csvWriter.writerows(lista)
            except Exception as e:
                self.logObject.setMensaje('[walkFolderToCSV] Error: {}'.format(type(e).__name__))
            else:
                self.logObject.setMensaje('[walkFolderToCSV] Archivo {} creado correctamente'.format(getFname(filename,fecha)))
                #if not appendInAttachment(getFname(filename,fecha),os.path.abspath(getFname(filename,fecha))):
                if not appendInAttachment(getFname(filename,fecha),self.filesAttach):
                    self.logObject.setMensaje('[walkFolderToCSV] Archivo {} agregado a la lista de Attachments correctamente'.format(getFname(filename,fecha)))
                    self.filesAttach+=[[getFname(filename,fecha),f'{os.path.abspath(path)}{self.__folderLimiter}{getFname(filename,fecha)}']]
                else:
                    self.logObject.setMensaje('[walkFolderToCSV] Archivo {} ya existe en la lista de Attachments. No se modifica...'.format(getFname(filename,fecha)))
                return [getFname(filename,fecha),f'{os.path.abspath(path)}{self.__folderLimiter}{getFname(filename,fecha)}']
        else:
            self.logObject.setMensaje('[walkFolderToCSV] La lista esta vacia. No se genera el archivo {}'.format(getFname(filename,fecha)))


def setEmailDict(folder,walkFolderObject,configObject):
    print('[setEmailDict] Generando la configuracion del Email...')
    emailDict={}
    emailDict['from']=configObject.mailconfig['credentials']['smtp']['user']
    emailDict['to']=','.join(configObject.walkConfig['carpetas'][folder]['to'])
    emailDict['subject']='[ {} ] Informe de {}'.format(datetime.date.today(), configObject.walkConfig['carpetas'][folder]['subject'])
    emailDict['body']='{}'.format(walkFolderObject.__str__())
    emailDict['body-encode']=configObject.bodyConfig['email']['settings']['body-encode']
    emailDict['body-type']=configObject.bodyConfig['email']['settings']['body-type']
    emailDict['attach'] = walkFolderObject.filesAttach
    return emailDict

def MimeFromYYaml(emailDict):
    msgMIMEMultipart = MIMEMultipart()
    msgMIMEMultipart['From']=emailDict['from']
    msgMIMEMultipart['To']=emailDict['to']
    msgMIMEMultipart['Subject']=emailDict['subject']
    try:
        msgMIMEMultipart.attach(MIMEText(emailDict['body'],emailDict['body-type'],emailDict['body-encode']))
    except Exception as e:
        print('[MimeFromYYaml] Error adjuntando el cuerpo del mensaje. Error: {}'.format(e))
    else:    
        if len(emailDict['attach']) > 0:
            for archivo in emailDict['attach']:
                try:
                    attachment=open(archivo[1],'rb')
                    p = MIMEBase('application', 'octet-stream')
                    p.set_payload((attachment).read())
                    encoders.encode_base64(p)
                    p.add_header('Content-Disposition', "attachment; filename= {}".format(archivo[0]))
                except Exception as e:
                    print('[MimeFormatYaml] No se pudo adjuntar el archivo {}. Error {}'.format(archivo[1],e))
                else:
                    msgMIMEMultipart.attach(p)
    return msgMIMEMultipart

def enviarEmail(configObject,folder,cuerpo=MIMEMultipart()):
    emails=configObject.walkConfig['carpetas'][folder]['to']
    contador=0
    for email in emails:
        contador+=1
        print('[enviarEmail] Enviando email a {} {} / {} emails enviados'.format(email,contador,len(emails)))
        server = smtplib.SMTP(configObject.mailconfig['credentials']['smtp']['host'],int(configObject.mailconfig['credentials']['smtp']['port']))
        server.ehlo()
        server.starttls()
        server.login(configObject.mailconfig['credentials']['smtp']['user'], configObject.mailconfig['credentials']['smtp']['pass'])
        server.sendmail(cuerpo['From'],email,cuerpo.as_string())    

""" config = configuraciones()
carpetaConfig = 'exp_revisar'
midNombreArchivo = '_'.join(config.walkConfig['carpetas'][carpetaConfig]['path'].split('/')[-1].split())
logObject = logToFile(config.appSettings['settings']['logsFolder'],midNombreArchivo)
walkFolderObject = walkFolder(config.walkConfig['carpetas'][carpetaConfig],logObject)
walkFolderObject.setFileList()
walkFolderObject.analizeFiles()
walkFolderObject.setWrongFileNameList()
walkFolderObject.setDuplicates()
walkFolderObject.setPdfNoConformance()
walkFolderObject.listToCSV(walkFolderObject.pdfDuplicados,midNombreArchivo+'-pdf-duplicados',path=config.appSettings['settings']['csvFolder'])
walkFolderObject.listToCSV(walkFolderObject.pdfErrorFirma,midNombreArchivo+'-pdf-errorFirma',path=config.appSettings['settings']['csvFolder'])
walkFolderObject.listToCSV(walkFolderObject.pdfRaw,midNombreArchivo+'-raw',path=config.appSettings['settings']['csvFolder'])
walkFolderObject.listToCSV(walkFolderObject.pdfErrorNombre,midNombreArchivo+'-pdf-nombre',path=config.appSettings['settings']['csvFolder'])
walkFolderObject.listToCSV(walkFolderObject.otherFiles,midNombreArchivo+'-no-pdf',path=config.appSettings['settings']['csvFolder'])
walkFolderObject.listToCSV(walkFolderObject.pdfNoConformance,midNombreArchivo+'-no-pdfA',path=config.appSettings['settings']['csvFolder'])
emailDict = setEmailDict(carpetaConfig,walkFolderObject,config)
msgMIMEMultipart = MimeFromYYaml(emailDict)
enviarEmail(config,'exp_ingresar',msgMIMEMultipart)
#enviarEmail(config,carpetaConfig,msgMIMEMultipart)
"""

def body(walkFolderObject):
    bodyText = 'Ocurrencia de Archivos(por extensiones): \n\n'
    bodyText += f'Pdf: {len(walkFolderObject.pdfRaw)}\n'
    bodyText += f'No Pdf/A: {len(walkFolderObject.pdfNoConformance)}\n'
    bodyText += f'Pdf con Error de Firmas: {len(walkFolderObject.pdfErrorFirma)}\n'
    bodyText += f'Pdf con Error de Nombres: {len(walkFolderObject.pdfErrorNombre)}\n'
    bodyText += f'Pdfs Duplicados: {len(walkFolderObject.pdfDuplicados)}\n'
    listaEtensioneArchivo = Counter([ file[1].split('.')[-1].lower() for file in walkFolderObject.otherFiles ])
    for item in listaEtensioneArchivo:
        bodyText += f'{item}: {listaEtensioneArchivo[item]}\n'
    if len(walkFolderObject.pdfErrorFirma)>1:
        bodyText += f'\n\nArchivos PDF Con error en Firma Digital:\n'
        for file in walkFolderObject.pdfErrorFirma:
            bodyText += f'{file[0]}/{file[1]}\n'
    if len(walkFolderObject.pdfNoConformance)>1:
        bodyText += f'\n\nArchivos PDF que no son A:\n'
        for file in walkFolderObject.pdfNoConformance:
            bodyText += f'{file[0]}/{file[1]}\n'
    if len(walkFolderObject.pdfDuplicados)>1:
        claves = list(Counter([file[4] for file in walkFolderObject.pdfDuplicados[1:]]).keys())
        bodyText += f'\n\nArchivos Duplicados\n'
        for clave in claves:
            bodyText += f'\nMD5: {clave}:\n'
            for file in walkFolderObject.pdfDuplicados:
                if file[4] == clave:
                    bodyText += f'{file[0]}/{file[1]}\n'
    return bodyText

def main():
    config = configuraciones()
    #carpetaConfig = 'exp_ingresar'
    # Filename
    for carpetaConfig in config.walkConfig['carpetas']:
        if platform.system()=='Windows':
            print('Analizando carpeta: {}'.format(config.walkConfig['carpetas'][carpetaConfig]['path'].split('\\')[-1]))
            midNombreArchivo = '_'.join(config.walkConfig['carpetas'][carpetaConfig]['path'].split('\\')[-1].split())
        else:
            print('Analizando carpeta: {}'.format(config.walkConfig['carpetas'][carpetaConfig]['path'].split('/')[-1]))
            midNombreArchivo = '_'.join(config.walkConfig['carpetas'][carpetaConfig]['path'].split('/')[-1].split())
        midNombreArchivo = '_'.join(config.walkConfig['carpetas'][carpetaConfig]['path'].split('/')[-1].split())
        logObject = logToFile(config.appSettings['settings']['logsFolder'],midNombreArchivo)
        walkFolderObject = walkFolder(config.walkConfig['carpetas'][carpetaConfig],logObject)
        walkFolderObject.setFileList()
        walkFolderObject.analizeFiles()
        walkFolderObject.setWrongFileNameList()
        walkFolderObject.setDuplicates()
        walkFolderObject.setPdfNoConformance()
        walkFolderObject.listToCSV(walkFolderObject.pdfDuplicados,midNombreArchivo+'-pdf-duplicados',path=config.appSettings['settings']['csvFolder'])
        walkFolderObject.listToCSV(walkFolderObject.pdfErrorFirma,midNombreArchivo+'-pdf-errorFirma',path=config.appSettings['settings']['csvFolder'])
        walkFolderObject.listToCSV(walkFolderObject.pdfRaw,midNombreArchivo+'-raw',path=config.appSettings['settings']['csvFolder'])
        walkFolderObject.listToCSV(walkFolderObject.pdfErrorNombre,midNombreArchivo+'-pdf-nombre',path=config.appSettings['settings']['csvFolder'])
        walkFolderObject.listToCSV(walkFolderObject.otherFiles,midNombreArchivo+'-no-pdf',path=config.appSettings['settings']['csvFolder'])
        walkFolderObject.listToCSV(walkFolderObject.pdfNoConformance,midNombreArchivo+'-no-pdfA',path=config.appSettings['settings']['csvFolder'])
        walkFolderObject.listToCSV([['Carpeta','Archivo','MD5']]+[[archivo[0],archivo[1],archivo[4]] for archivo in walkFolderObject.pdfRaw],midNombreArchivo+'-paraEnviar',path=config.appSettings['settings']['csvFolder'])
        emailDict = setEmailDict(carpetaConfig,walkFolderObject,config)
        msgMIMEMultipart = MimeFromYYaml(emailDict)
        #enviarEmail(config,'exp_ingresar',msgMIMEMultipart)
        enviarEmail(config,carpetaConfig,msgMIMEMultipart)
    del(logObject)
    del(walkFolderObject)

if __name__ == "__main__":
    main()
