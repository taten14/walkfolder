import time
import signal

def timeout(seconds_before_timeout):
    def decorate(f):
        def handler(signum, frame):
            raise TimeoutError()
        def new_f(*args, **kwargs):
            old = signal.signal(signal.SIGALRM, handler)
            old_time_left = signal.alarm(seconds_before_timeout)
            if 0 < old_time_left < seconds_before_timeout: # never lengthen existing timer
                signal.alarm(old_time_left)
            start_time = time.time()
            try:
                result = f(*args, **kwargs)
            finally:
                if old_time_left > 0: # deduct f's run time from the saved timer
                    old_time_left -= time.time() - start_time
                signal.signal(signal.SIGALRM, old)
                signal.alarm(old_time_left)
            return result
        new_f.__name__ = f.__name__
        return new_f
    return decorate


path = '/home/TRIBUNALCBA/dpaniagua/winSectores/Rendicion de Cuentas/00 Expedientes a ingresar en SGE/00 Casa Central/SERVICIO PENITENCIARIO/SERVICIO PENITENCIARIO FONDOS'
file = '0011-060260-2020 FP 15-10-2020.pdf'
@timeout(5)
def leer_archivo(pdfPath,pdfFile):
    archivo(f'{pdfPath}',f'{pdfFile}')

#https://www.chrisjhart.com/Windows-10-ssh-copy-id/

def main():
    arvhivo = '0081-039154-2020 09-10-2020.pdf'
    carpetaLinux = "/home/TRIBUNALCBA/dpaniagua/winSectores/Rendicion de Cuentas/00 Expedientes a ingresar en SGE"
    carpetaWindows = '\\\\172.16.10.3\\Sectores\\Rendicion de Cuentas\\00 Expedientes a ingresar en SGE'
    folderTuple = os.walk('{}'.format(carpetaLinux))
    with concurrent.futures.ThreadPoolExecutor(max_workers=3) as executor:
        executor.map(archivo)

# Comentar
# CTRL K CTRL C

# Descomentar
# CTRL K CTRL U