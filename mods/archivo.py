#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
from sys import exit as sysexit
import re
import datetime
import time
import csv
from types import GetSetDescriptorType
import yaml
from email.mime.multipart import MIMEMultipart 
from email.mime.text import MIMEText 
from email.mime.base import MIMEBase 
from email import encoders
import smtplib
import hashlib
from collections import Counter
from PyPDF2 import PdfFileReader
import signal

def timeout(seconds_before_timeout):
    def decorate(f):
        def handler(signum, frame):
            raise TimeoutError()
        def new_f(*args, **kwargs):
            old = signal.signal(signal.SIGALRM, handler)
            old_time_left = signal.alarm(seconds_before_timeout)
            if 0 < old_time_left < seconds_before_timeout: # never lengthen existing timer
                signal.alarm(old_time_left)
            start_time = time.time()
            try:
                result = f(*args, **kwargs)
            finally:
                if old_time_left > 0: # deduct f's run time from the saved timer
                    old_time_left -= time.time() - start_time
                signal.signal(signal.SIGALRM, old)
                signal.alarm(old_time_left)
            return result
        new_f.__name__ = f.__name__
        return new_f
    return decorate


class archivo():
    def __init__(self,filePath,fileName,logObject=None,tempPath=None):
        self.fileName = fileName
        self.filePath = filePath
        self.pdfCTime=None
        self.pdfMTime=None
        self.osCTime=None
        self.osMTime=None
        self.__filePointer = self.__setFilePointer()
        self.__pdfObject = self.__setPDFObject()
        self.MD5=self.__setMD5()
        self.signed = self.__setSignInfo()
        self.logObject = logObject
        self.pdfAConformance = self.__setPDFConformance()
    def __del__(self):
        print(f'Cerrando archivo {self.filePath}/{self.fileName}...')
        self.__filePointer.close()
    def __str__(self):
        #print('Archivo: {}\nPath: {}\nMD5: {}\nPDF/A: {}\nFirmado por: {}\nFecha de Firma: {}'.format(self.fileName,self.filePath,self.MD5,self.pdfAConformance,self.signed[0][0],self.signed[0][1]))
        return f'---------------------------------------\nArchivo: {self.fileName}\nPath: {self.filePath}\nMD5: {self.MD5}\nPDF/A: {self.pdfAConformance}\nFirmado por: {self.signed[0][0]}\nFecha de Firma: {self.signed[0][1]}'
    def __setFilePointer(self):
        try:
            print(f'Abriendo archivo {self.filePath}/{self.fileName}')
            filePointer = open(f'{self.filePath}/{self.fileName}','rb')
        except Exception as e:
            print(f'No se pudo abrir el archivo: \nPath: {self.filePath}\nArchivo:{self.fileName}\nError: {type(e).__name__}')
        else:
            return filePointer
    @timeout(5)
    def __setPDFObject(self):
        try:
            pdfObject = PdfFileReader(self.__filePointer)
        except Exception as e:
            print(f'PDF No valido: \nPath: {self.filePath}\nArchivo:{self.fileName}\nError: {type(e).__name__}')
            return None
        else:
            return pdfObject
    def isPdf(self):
        return True if self.__pdfObject is not None else False
    def isSigned(self):
        if self.isPdf():
            return True if self.__pdfObject.getFields() else False
        else:
            print(f'PDF No valido: \nPath: {self.filePath}\nArchivo:{self.fileName}')
            return None
    def __setSignInfo(self):
        if self.isPdf() and self.isSigned():
            fecha = lambda f: f'{f[:4]}-{f[4:6]}-{f[6:8]} {f[8:10]}:{f[10:12]}'
            nombre = lambda m: m['/V']['/Name']  if '/Name' in m['/V'] else None
            return [[nombre(self.__pdfObject.getFields()[firma]),fecha(self.__pdfObject.getFields()[firma]['/V']['/M'][2:-7])] for firma in self.__pdfObject.getFields()]
        else:
            return None
    def __setMD5(self):
        if self.isPdf():    
            #f = lambda x: x.read(4096)
            try:
                #self.logObject.setMensaje('[getMD5Hash] Generando MD5 para {}'.format(filename))
                hash_md5 = hashlib.md5()
                with open(f'{self.filePath}/{self.fileName}', 'rb') as punteroBinario:
                    for chunk in iter(lambda: punteroBinario.read(4096), b""):
                        hash_md5.update(chunk)
                #self.logObject.setMensaje('[getMD5Hash] hash: {}. Finalizado...'.format(hash_md5.hexdigest()))
                return hash_md5.hexdigest()
            except Exception as e:
                #self.logObject.setMensaje('[getMD5Hash] Error {} al procesar archivo {}'.format(type(e).__name__,filename))
                return f'MD5 Error: {type(e).__name__}'
        else:
            return None
    def __setPDFConformance(self):
        if self.isPdf() and self.isSigned():
            cadena = r'pdfaid\:conformance[\=|\>][\"]*([\w])*'
            resBusq = re.search(cadena,self.__pdfObject.xmpMetadata.rdfRoot.toxml())
            if resBusq:
                txtEncontrado = self.__pdfObject.xmpMetadata.rdfRoot.toxml()[resBusq.start():resBusq.end()]
                pdfVersion = txtEncontrado.replace('>','=') if txtEncontrado[-2] == '>' else txtEncontrado.replace('="','=')
                #print('{}/{} | {}'.format(self.filePath,self.fileName,pdfVersion))
                return pdfVersion
    def full_toCsv(self):
        '''
        Envia todos los campos para meterlos en el archivo raw csv
        '''
        return [self.filePath,self.fileName,self.pdfCTime,self.pdfMTime,self.MD5,self.pdfAConformance]
    def signToCSV(self):
        if self.isSigned():
            return [[self.filePath,self.fileName,firma[0],firma[1]] for firma in self.signed]

def main():
    arvhivo = '0081-039154-2020 09-10-2020.pdf'
    carpetaLinux = "/home/TRIBUNALCBA/dpaniagua/winSectores/Rendicion de Cuentas/00 Expedientes a ingresar en SGE"
    carpetaWindows = '\\\\172.16.10.3\\Sectores\\Rendicion de Cuentas\\00 Expedientes a ingresar en SGE'
    folderTuple = os.walk('{}'.format(carpetaLinux))
    # cadena = '(pdfaid\:conformance[\=|\>][\"]*[\w][\"]*)'
    # cadena = 'pdfaid\:conformance[\=|\>][\"]*([\w])*'
    # fecha = lambda f: '{}-{}-{} {}:{}'.format(f[:4],f[4:6],f[6:8],f[8:10],f[10:12])
    for fileTuple in folderTuple:
        if len(fileTuple[2])>1:
            for file in fileTuple[2]:
                pdf = archivo(fileTuple[0],file)
                print('CSV: ',pdf.full_toCsv())
                print('Firmas: ',pdf.signToCSV())
                #if pdf.isPdf() and pdf.isSigned():
                #    print(pdf)
                del(pdf)

if __name__ == "__main__":
    main()