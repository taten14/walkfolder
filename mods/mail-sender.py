# -*- coding: utf-8 -*-
#!/usr/bin/python3

import os
from sys import exit as sysexit
import re
import datetime
import time
import csv
from types import GetSetDescriptorType
import yaml
from email.mime.multipart import MIMEMultipart 
from email.mime.text import MIMEText 
from email.mime.base import MIMEBase 
from email import encoders
import smtplib
import hashlib
from collections import Counter
import platform

def getMailCredentials(file=".\\mailconfig.yml"):
    try:
        with open(file,'r',encoding='utf-8') as yamlFilePointer:
            mailConfigDict = yaml.load(yamlFilePointer, Loader=yaml.FullLoader)
    except Exception as e:
        print('[getMailCredentials] Error: {}'.format(type(e).__name__))
    else:
        return mailConfigDict


def getMailSettings(file='.\\body.yml'):
    try:
        with open(file,'r',encoding='utf-8') as yamlFilePointer:
            bodyConfigDict = yaml.load(yamlFilePointer, Loader=yaml.FullLoader)
    except Exception as e:
        print('[getMailSettings] Error: {}'.format(type(e).__name__))
    else:
        return bodyConfigDict


def setEmailDict(folder,para=['dante.paniagua@tcpcordoba.gov.ar','taten14@gmail.com'],\
        de='alertas@tcpcordoba.gov.ar',\
        asunto='[Prueba] prueba',\
        cuerpo='Prueba',\
        filename=None):
    print('[setEmailDict] Generando la configuracion del Email...')
    walkFolderObject = walkFolder(folder)
    walkFolderList = walkFoldertoList(walkFolderObject)
    csvFileList=walkFolderToCSV(walkFolderList,filename)
    mailSettings=getMailSettings()
    emailDict={}
    emailDict['from']=de
    emailDict['to']=para
    emailDict['subject']=asunto
    emailDict['body']='{}'.format(cuerpo)
    emailDict['body-encode']=mailSettings['email']['settings']['body-encode']
    emailDict['body-type']=mailSettings['email']['settings']['body-type']
    emailDict['attach'] = [csvFileList]
    return emailDict


def MimeFromYYaml(emailDict):
    msgMIMEMultipart = MIMEMultipart()
    msgMIMEMultipart['From']=emailDict['from']
    msgMIMEMultipart['To']=','.join(emailDict['to'])
    msgMIMEMultipart['Subject']=emailDict['subject']
    try:
        msgMIMEMultipart.attach(MIMEText(emailDict['body'],emailDict['body-type'],emailDict['body-encode']))
    except Exception as e:
        print('[MimeFromYYaml] Error adjuntando el cuerpo del mensaje. Error: {}'.format(e))
    else:    
        if len(emailDict['attach']) > 0:
            for archivo in emailDict['attach']:
                try:
                    attachment=open(archivo[1],'rb')
                    p = MIMEBase('application', 'octet-stream')
                    p.set_payload((attachment).read())
                    encoders.encode_base64(p)
                    p.add_header('Content-Disposition', "attachment; filename= {}".format(archivo[0]))
                except Exception as e:
                    print('[MimeFormatYaml] No se pudo adjuntar el archivo {}. Error {}'.format(archivo[1],e))
                else:
                    msgMIMEMultipart.attach(p)
    return msgMIMEMultipart


def enviarEmail(cuerpo=MIMEMultipart()):
    emails=cuerpo['To'].split(',')
    contador=0
    mailCredentials = getMailCredentials()
    for email in emails:
        contador+=1
        print('[enviarEmail] Enviando email a {} {} / {} emails enviados'.format(email,contador,len(emails)))
        server = smtplib.SMTP(mailCredentials['credentials']['smtp']['host'],int(mailCredentials['credentials']['smtp']['port']))
        server.ehlo()
        server.starttls()
        server.login(mailCredentials['credentials']['smtp']['user'], mailCredentials['credentials']['smtp']['pass'])
        server.sendmail(cuerpo['From'],email,cuerpo.as_string())

if __name__ == "__main__":
    pass