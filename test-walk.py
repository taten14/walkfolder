# -*- coding: utf-8 -*-
#!/usr/bin/python3

import os
from sys import exit as sysexit
import re
import datetime
import time
import csv
from types import GetSetDescriptorType
import yaml
from email.mime.multipart import MIMEMultipart 
from email.mime.text import MIMEText 
from email.mime.base import MIMEBase 
from email import encoders
import smtplib
import hashlib
from collections import Counter
import platform


class configuraciones():
    def __init__(self,bodyConfigFile='./body.yml',walkConfigFile='./carpetas.yml',carpetasConfigFile='./body.yml',appSettingsFile='./appsettings.yml',mailSettingsFile='./mailconfig.yml'):
        self.bodyConfig = self.__getSettings(bodyConfigFile)
        self.walkConfig = self.__getSettings(walkConfigFile)
        self.appSettings = self.__getSettings(appSettingsFile)
        self.__mailconfig = self.__getSettings(mailSettingsFile)
    def __str__(self):
        return('{}\n{}\n{}\n{}'.format(self.bodyConfig,self.walkConfig,self.walkConfig,self.appSettings))
    def __getSettings(self,file):
        try:
            print('[getSettings] Cargando archivo de configuracion {}'.format(file))
            if platform.system()=='Windows':
                with open(file,'r',encoding='utf-8') as yamlFilePointer:
                    appSettings = yaml.load(yamlFilePointer, Loader=yaml.FullLoader)
            else:
                with open(file,'r',encoding='utf-8') as yamlFilePointer:
                    appSettings = yaml.load(yamlFilePointer)
        except Exception as e:
            print('[getSettings] Error: {}'.format(type(e).__name__))
        else:
            return appSettings

class logToFile():
    def __init__(self,folder,filename):
        self.folder = folder
        self.logFileName = self.getFileName(filename)
        self.__folderLimiter ='\\' if platform.system()=='Windows' else '/'
    def __str__(self):
        return(f'{self.folder}{self.__folderLimiter}{self.logFileName}')
    def getFileName(self,filename):
        fechaArchivo = datetime.datetime.now().strftime('%Y%m%d')
        return '{}-{}.log'.format(fechaArchivo,filename) if filename.split('.')[-1] != 'log' else '{}-{}'.format(fechaArchivo,filename)
    def setLogFolder(self):
        try:
            print('[createFolder] Intentando crear la carpeta {}.'.format(self.folder))
            os.mkdir(self.folder)
            print('[createFolder] Carpeta {} creada con exito.'.format(self.folder))
        except Exception as e:
            print('[createFolder] Error: {} No se pudo crear la carpeta {}. Saliendo del programa'.format(type(e).__name__,self.folder))
            sysexit() # es una buena practica??
    def setMensaje(self,mensaje):
        if not os.path.isdir(self.folder):
            self.setLogFolder()
        timeStamp = '{}'.format(datetime.datetime.now().strftime('%Y%m%d-%H%M%S'))
        with open(f'{self.folder}{self.__folderLimiter}{self.logFileName}','a+',encoding='utf-8',newline='') as filePointer:
            filePointer.write('[{}] {}\n'.format(timeStamp,mensaje))


class walkFolder():
    def __init__(self,folderDict,logObject):
        self.__folderLimiter = '\\' if platform.system()=='Windows' else '/'
        # Recibe un diccionario de la carpeta a procesar [carpeta][nombre_carpeta] resultado de procesar el archivo carpetas.yml
        self.path=folderDict['path']
        self.extensiones=folderDict['extensiones']
        self.regExpr = folderDict['validador']
        self.winPath = folderDict['winPath']
        self.walkFolderObject = self.__walkFolder()
        self.logObject = logObject
        self.walkFolderList = self.walkFoldertoList(self.extensiones)
        self.filesAttach = []
        self.listaRaw = []
        self.listaDuplicados = []
        self.listaNombres = []
    def __str__(self):
        return '{}'.format(self.path)
    def __walkFolder(self):
        if not os.path.isdir(self.path) | os.path.ismount(self.path):
            print('[walkFolder] Error: El directorio {} no existe o no esta montado'.format(self.path))
        else:
            try:
                walkFolderObject = os.walk(self.path)
            except Exception as e:
                print('[walkFolder] Error: {}'.format(type(e).__name__))
                # Mandarme mail a mi porque fallo
            else:
                return walkFolderObject
    def __getMD5Hash(self,path,filename):
        try:
            self.logObject.setMensaje('[getMD5Hash] Generando MD5 para {}'.format(filename))
            hash_md5 = hashlib.md5()
            with open(f'{path}{self.__folderLimiter}{filename}', 'rb') as punteroBinario:
                for chunk in iter(lambda: punteroBinario.read(4096), b""):
                    hash_md5.update(chunk)
            self.logObject.setMensaje('[getMD5Hash] hash: {}. Finalizado...'.format(hash_md5.hexdigest()))
            return hash_md5.hexdigest()
        except Exception as e:
            self.logObject.setMensaje('[getMD5Hash] Error {} al procesar archivo {}'.format(type(e).__name__,filename))
            return 'MD5 Error: {}'.format(type(e).__name__)
    def __ocurrence(self):
        return dict(Counter([file[1].split('.')[-1].lower() for file in [folder for folder in self.walkFolderList if folder[1]!='']]))
    def __folderFormatter(self,path,replace):
        return path.replace('/home/TRIBUNALCBA/dpaniagua/winSectores/Rendicion de Cuentas',replace).replace('/','\\')
    def getOcurrenceText(self):
        ocurrenceText='Archivos:\n\n'
        filesCounter = self.__ocurrence()
        for fileType in filesCounter:
            ocurrenceText+='\t- *.{}: {} archivos\n'.format(fileType,filesCounter[fileType])
        return ocurrenceText
    def setLogFolder(self,path):
        try:
            print(f'[createFolder] Intentando crear la carpeta {path}.')
            os.mkdir(path)
            print(f'[createFolder] Carpeta {path} creada con exito.')
        except Exception as e:
            print(f'[createFolder] Error: {type(e).__name__} No se pudo crear la carpeta {path}. Saliendo del programa')
            sysexit() # es una buena practica??
    def walkFoldertoList(self,includeList):
        limpiarCadena = lambda x: '{}'.format(x.encode('unicode-escape').decode().replace('\\\\', '\\'))
        getMTime = lambda x: datetime.datetime.fromtimestamp(os.path.getmtime(x))
        getCTime = lambda x: datetime.datetime.fromtimestamp(os.path.getctime(x))
        # Lambda que devuelve True si la extension esta en la lista de extensiones
        isIncluded = lambda file,includeList: True if file.split('.')[-1].lower() in includeList else False
        #walkFolderList=[['Carpeta','Archivo','Creacion','Modificacion','MD5']]
        walkFolderList=[] # La fila de nombres se asigna al momento del analisis
        totalTime=time.time()
        totalFiles=0
        t0=time.time()
        for folderTuple in self.walkFolderObject:
            self.logObject.setMensaje('[walkFoldertoList] Iniciando analisis de subcarpeta : {}'.format(limpiarCadena(folderTuple[0])))
            if len(folderTuple[2])==0 and len(folderTuple[1])==0:
                walkFolderList.append([f'{folderTuple[0]}','', getCTime(folderTuple[0]).__str__(), getMTime(folderTuple[0]).__str__(), ''])
            fileCounter=0
            for folderFile in folderTuple[2]:
                totalFiles+=1
                fileCounter+=1
                fName=f'{self.__folderLimiter}'.join([folderTuple[0], folderFile])
                if isIncluded(folderFile,includeList):
                    self.logObject.setMensaje('[walkFoldertoList] Analizando archivo {}'.format(folderFile))
                    walkFolderList.append([f'{folderTuple[0]}',\
                    limpiarCadena(folderFile),\
                    getCTime(fName).__str__(),\
                    getMTime(fName).__str__(),\
                    self.__getMD5Hash(folderTuple[0], folderFile)])
                else:
                    self.logObject.setMensaje('[walkFoldertoList] Omitiendo MD5 de archivo: {}'.format(folderFile))
                    walkFolderList.append([f'{limpiarCadena(folderTuple[0])}',\
                    limpiarCadena(folderFile),\
                    getCTime(fName).__str__(),\
                    getMTime(fName).__str__(),\
                    ''])
            self.logObject.setMensaje('[walkFoldertoList] Analisis de subcarpeta Finalizado. Tiempo: {:.0f} segundos. Archivos: {}'.format(time.time()-t0,fileCounter))
            fileCounter=0
            t0=time.time()
        self.logObject.setMensaje('[walkFoldertoList] Tiempo total transcurrido: {:.0f} segundos. Total de archivos procesados: {}'.format(time.time()-totalTime,totalFiles))
        return walkFolderList
    def listToCSV(self, lista,filename,path='./'): #Modificar. Yo quiero enviar la lista que se va a escribir en el CSV
        if not os.path.isdir(path):
            self.setLogFolder(path)
        fecha=datetime.datetime.now().strftime("%Y%m%d")
        getFname = lambda fname,fecha: '{}-{}.csv'.format(fecha,fname) if fname.split('.')[-1].lower() != 'csv' else '{}-{}'.format(fecha,fname)
        if len(lista) > 1:
            appendInAttachment = lambda file, fileList: True if file in fileList else False
            #getFname = lambda fname,fecha: '{}-{}.csv'.format(fecha,fname) if fname.split('.')[-1].lower() != 'csv' else '{}-{}'.format(fecha,fname)
            try:
                print(f'{path}{getFname(filename,fecha)}')
                with open(f'{path}{getFname(filename,fecha)}','w+',newline ='',encoding='utf-8') as csvFile:
                    csvWriter=csv.writer(csvFile)
                    csvWriter.writerows(lista)
            except Exception as e:
                self.logObject.setMensaje('[walkFolderToCSV] Error: {}'.format(type(e).__name__))
            else:
                self.logObject.setMensaje('[walkFolderToCSV] Archivo {} creado correctamente'.format(getFname(filename,fecha)))
                #if not appendInAttachment(getFname(filename,fecha),os.path.abspath(getFname(filename,fecha))):
                if not appendInAttachment(getFname(filename,fecha),self.filesAttach):
                    self.logObject.setMensaje('[walkFolderToCSV] Archivo {} agregado a la lista de Attachments correctamente'.format(getFname(filename,fecha)))
                    self.filesAttach+=[[getFname(filename,fecha),f'{os.path.abspath(path)}{self.__folderLimiter}{getFname(filename,fecha)}']]
                else:
                    self.logObject.setMensaje('[walkFolderToCSV] Archivo {} ya existe en la lista de Attachments. No se modifica...'.format(getFname(filename,fecha)))
                return [getFname(filename,fecha),f'{os.path.abspath(path)}{self.__folderLimiter}{getFname(filename,fecha)}']
        else:
            self.logObject.setMensaje('[walkFolderToCSV] La lista esta vacia. No se genera el archivo {}'.format(getFname(filename,fecha)))
    def getWrongFileNameList(self):
        isIncluded = lambda file,includeList: True if file.split('.')[-1].lower() in includeList else False
        FileList = []
        if len(self.regExpr) > 0:
            for validator in self.regExpr:
                FileList += [[self.__folderFormatter(file[0],self.winPath),file[1],file[2],file[3],file[4],validator] for file in self.walkFolderList if len(file[1])>0]
                #FileList += [file+[validator] for file in self.walkFolderList if not re.match(validator,file[1]) and len(file[1])>0 and isIncluded(file[1],self.extensiones)]
        self.listaNombres = [['Carpeta','Archivo','Creacion','Modificacion','MD5','Filtro']]+FileList
        return self.listaNombres
    def getRaw(self):
        self.listaRaw = [['Carpeta','Archivo','Creacion','Modificacion','MD5']]+[[self.__folderFormatter(file[0],self.winPath),file[1],file[2],file[3],file[4]] for file in self.walkFolderList]
        return self.listaRaw
    def getDuplicates(self):
        #Recibe la lista completa de archivos
        # devuelve una lista de archivos duplicados
        #listaDuplicados = lambda fileMD5,walkFolderList: [[file] for file in walkFolderList if file[4] == fileMD5 and not fileMD5 == '']
        listaDuplicados = lambda fileMD5,walkFolderList: [[file] for file in walkFolderList if file[4] == fileMD5 and not fileMD5 == '']
        listaOrdenada = [[self.__folderFormatter(dupFile[0],self.winPath),dupFile[1],dupFile[2],dupFile[3],dupFile[4],self.regExpr] for dupFile in self.walkFolderList if len(listaDuplicados(dupFile[4],self.walkFolderList))>1]
        # Ahora la lista se devuelve ordenada por MD5
        self.listaDuplicados = [['Carpeta','Archivo','Creacion','Modificacion','MD5']]+sorted(listaOrdenada,key=lambda x: x[4])
        return self.listaDuplicados
    def getWalkFolderList(self):
        walkFolderList=[['Carpeta','Archivo','Creacion','Modificacion','MD5']]
        return walkFolderList + self.walkFolderList



def main():
    config = configuraciones()
    carpetaConfig = 'exp_ingresar'
    # Filename
    for carpetaConfig in config.walkConfig['carpetas']:
        if platform.system()=='Windows':
            print('Analizando carpeta: {}'.format(config.walkConfig['carpetas'][carpetaConfig]['path'].split('\\')[-1]))
            midNombreArchivo = '_'.join(config.walkConfig['carpetas'][carpetaConfig]['path'].split('\\')[-1].split())
        else:
            print('Analizando carpeta: {}'.format(config.walkConfig['carpetas'][carpetaConfig]['path'].split('/')[-1]))
            midNombreArchivo = '_'.join(config.walkConfig['carpetas'][carpetaConfig]['path'].split('/')[-1].split())
        logObject = logToFile(config.appSettings['settings']['logsFolder'],midNombreArchivo)
        walkFolderObject = walkFolder(config.walkConfig['carpetas'][carpetaConfig],logObject)
        walkFolderObject.listToCSV(walkFolderObject.getRaw(),midNombreArchivo+'-raw',path=config.appSettings['settings']['csvFolder'])
        walkFolderObject.listToCSV(walkFolderObject.getWrongFileNameList(),midNombreArchivo+'-error_nombres',path=config.appSettings['settings']['csvFolder'])
        walkFolderObject.listToCSV(walkFolderObject.getDuplicates(),midNombreArchivo+'-duplicados',path=config.appSettings['settings']['csvFolder'])
    del(logObject)
    del(walkFolderObject)

main()